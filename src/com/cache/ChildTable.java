package com.cache;

import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.HashMap;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

public class ChildTable {

	private final int id;
	private final HashMap<Integer, Child> children;

	public ChildTable(int id, Child... children) {
		this.children = new HashMap<>(children.length);
		this.id = id;
		for (Child child : children)
			this.children.put(child.getId(), child);
	}

	public static ChildTable decode(ByteBuffer buffer) {
		int tableId = buffer.get();
		Child[] children = new Child[buffer.getShort()];
		for (int i = 0; i < children.length; i++) {
			int id = buffer.getShort();
			byte[] childData = new byte[buffer.getInt()];
			buffer.get(childData);
			children[i] = new Child(id, childData);
		}
		return new ChildTable(tableId, children);
	}

	public byte[] encode() {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeByte(id);
		out.writeShort(children.size());
		for (Child child : children.values()) {
			out.writeShort(child.getId());
			out.writeInt(child.getData().length);
			out.write(child.getData());
		}
		return out.toByteArray();
	}

	public int getId() {
		return id;
	}

	public Collection<Child> getChildren() {
		return children.values();
	}

	public void addChild(Child child) {
		this.children.put(child.getId(), child);
	}

	public void removeChild(int id) {
		this.children.remove(id);
	}

	public int missingChildIndex() {
		for (int i = 0; i < children.size(); i++)
			if (children.get(i) == null)
				return i;
		return children.size();
	}

	@Override
	public String toString() {
		return "Table " + id;
	}

}
