package com.cache;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.HashMap;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.lib.Log;

public class CacheStore {

	private HashMap<Integer, ChildTable> tables;

	public CacheStore(ChildTable... tables) {
		this.tables = new HashMap<>(tables.length);
		for (ChildTable table : tables)
			this.tables.put(table.getId(), table);
	}

	public static CacheStore load(InputStream in) throws Exception {
		Log.info("Loading Cache...");
		ByteBuffer out = ByteBuffer.wrap(ByteStreams.toByteArray(in));
		ChildTable[] tables = new ChildTable[out.getShort()];
		int childSize = 0;
		for (int tableId = 0; tableId < tables.length; tableId++) {
			byte[] tableData = new byte[out.getInt()];
			out.get(tableData);
			ChildTable table = ChildTable.decode(ByteBuffer.wrap(tableData));
			tables[table.getId()] = table;
			childSize += table.getChildren().size();
		}
		Log.info("Loaded " + tables.length + " tables from cache with a total of " + childSize + " children.");
		return new CacheStore(tables);
	}

	@SuppressWarnings("resource")
	public void writeToFile(File file) throws Exception {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeShort(tables.size());
		for (ChildTable table : tables.values()) {
			byte[] tableData = table.encode();
			out.writeInt(tableData.length);
			out.write(tableData);
		}
		new FileOutputStream(file).write(out.toByteArray());
	}

	public Collection<ChildTable> getTables() {
		return tables.values();
	}

	public void addTable(ChildTable table) {
		this.tables.put(table.getId(), table);
	}

	@Override
	public String toString() {
		return "Cache Store";
	}
}
