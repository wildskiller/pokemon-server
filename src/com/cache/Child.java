package com.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import com.google.common.io.ByteStreams;

public class Child {

	private final int id;
	private byte[] data;

	public Child(int id, byte[] data) {
		this.id = id;
		this.data = data;
	}

	public int getId() {
		return id;
	}

	public byte[] getData() {
		return data;
	}

	public void loadDataFromFile(File file) {
		try {
			loadDataFromStream(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void loadDataFromStream(InputStream stream) {
		try {
			this.data = ByteStreams.toByteArray(stream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		return "Child " + id;
	}

}
