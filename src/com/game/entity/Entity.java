package com.game.entity;

import com.game.pathfinding.AStarPathFinder;
import com.game.pathfinding.Location;
import com.game.pathfinding.PathFinder;
import com.lib.config.ConfigSection;

/**
 * @author Albert Beaupre
 */
public abstract class Entity {

	private final ConfigSection configuration;

	private Location location;
	private PathFinder pathFinder;

	/**
	 * Constructs a new {@code Entity}.
	 */
	public Entity() {
		this.location = new Location(3222, 3222, 0);
		this.configuration = new ConfigSection();
		this.pathFinder = new AStarPathFinder();
	}

	/**
	 * Retrieves a configuration value from this {@code Entity} with the specified {@code key}. If
	 * the value isn't existent, then the specified {@code fallback} will be returned instead.
	 * 
	 * @param key
	 *            the key associated with the value placed in configuration
	 * @param fallback
	 *            the value to fall back on if the value is non-existent
	 * @return the value associated with the key; otherwise return fallback
	 */
	@SuppressWarnings("unchecked")
	public <T> T config(Object key, T fallback) {
		return (T) configuration.getOrDefault(key, fallback);
	}

	public PathFinder getPathFinder() {
		return pathFinder;
	}

	public void setPathFinder(PathFinder pathFinder) {
		this.pathFinder = pathFinder;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

}
