package com.game.pathfinding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import com.game.entity.Entity;

public class AStarPathFinder implements PathFinder {

	@Override
	public List<Location> findPath(Entity entity, Location endPoint) {
		ArrayList<Location> open = new ArrayList<>();
		HashSet<Location> closed = new HashSet<>();
		final Location start = new Location(entity.getLocation().getX(), entity.getLocation().getY(), entity.getLocation().getZ());
		final Location goal = new Location(endPoint.getX(), endPoint.getY(), endPoint.getZ());
		if (start.getZ() != goal.getZ() || start.equals(goal))
			return Collections.emptyList();

		start.heuristic = heuristic(start, goal);
		open.add(start);

		while (open.size() > 0) {
			Collections.sort(open, (o1, o2) -> Double.compare(o1.getF(), o2.getF()));
			Location current = open.get(0);
			if (current.equals(goal))
				return construct(start, current);
			open.remove(current);
			closed.add(current);
			for (Location neighbor : current.getNeighbors(this, goal, 1)) {
				if (closed.contains(neighbor))
					continue;

				if (neighbor.equals(goal))
					return construct(start, neighbor);
				double tentative_cost = current.cost + current.distance(neighbor);
				if (!open.contains(neighbor) || tentative_cost < neighbor.cost) {
					neighbor.cost = tentative_cost;
					if (!open.contains(neighbor)) {
						open.add(neighbor);
					}
				}
			}
		}
		return Collections.emptyList();
	}

	private List<Location> construct(Location start, Location goal) {
		List<Location> path = new ArrayList<>();
		Location last = goal;
		while (!last.equals(start)) {
			path.add(last);
			last = last.parent;
		}
		Collections.reverse(path);
		return path;
	}

	@Override
	public boolean blocked(Location from, Location to) {
		return false;
	}

	@Override
	public double heuristic(Location from, Location to) {
		double dx = Math.abs(from.getX() - to.getX());
		double dy = Math.abs(from.getY() - to.getY());
		return 1 * (dx * dx + dy * dy);
	}

}
