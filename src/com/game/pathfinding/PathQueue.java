package com.game.pathfinding;

import java.util.List;

public class PathQueue {

	private final List<Location> path;

	public PathQueue(List<Location> path) {
		this.path = path;
	}

	public Location nextNode() {
		if (path.size() == 0)
			return null;
		return path.remove(0);
	}

}
