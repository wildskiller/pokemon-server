package com.game.pathfinding;

import java.util.List;

import com.game.entity.Entity;

/**
 * @author Albert Beaupre
 */
public interface PathFinder {

	List<Location> findPath(Entity entity, Location goal);

	boolean blocked(Location from, Location to);

	double heuristic(Location from, Location to);
}
