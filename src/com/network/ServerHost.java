package com.network;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * @author Albert Beaupre
 */
public abstract class ServerHost {

	private final InetSocketAddress address;

	public abstract void connect(SocketChannel channel);

	public abstract void disconnect(SocketChannel channel);

	public abstract void encode(SocketChannel channel, ByteBuffer buffer);

	public abstract void decode(SocketChannel channel, ByteBuffer buffer);

	public ServerHost(String ip, int port) {
		this.address = new InetSocketAddress(ip, port);
	}

	public InetSocketAddress getAddress() {
		return address;
	}

}
