package com.network.message;

import java.nio.ByteBuffer;

import com.network.Session;

public interface MessageDecoder {

	void decode(Session session, ByteBuffer buffer) throws Exception;

}
