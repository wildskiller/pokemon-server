package com.network.message;

import java.nio.ByteBuffer;

import com.network.Session;

public interface MessageEncoder {

	void encode(Session session, ByteBuffer buffer) throws Exception;

}
