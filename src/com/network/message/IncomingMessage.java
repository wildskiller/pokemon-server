package com.network.message;

/**
 * @author Albert Beaupre
 */
public interface IncomingMessage {

	/**
	 * Returns the operation code of this message for identification.
	 * 
	 * @return the operation code
	 */
	int operationCode();

	/**
	 * Returns an array of bytes that is encoded or decoded based on the message type.
	 * 
	 * @return the array of bytes for this message
	 */
	byte[] data();
}
