package com.network.message.game;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.HashMap;

import com.core.Core;
import com.event.events.SessionConnectEvent;
import com.network.ServerHost;
import com.network.Session;

/**
 * @author Albert Beaupre
 */

public class GameHost extends ServerHost {

	private final HashMap<SocketChannel, Session> sessions;

	public GameHost(String host, int port) {
		super(host, port);
		this.sessions = new HashMap<>();
	}

	@Override
	public void connect(SocketChannel channel) {
		GameSession session = new GameSession(channel);
		sessions.put(channel, session);
		try {
			System.out.println("Client Connected: " + session.getChannel().getRemoteAddress());
			new SessionConnectEvent(Core.getServer(getAddress().getPort()), session, true).call();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void disconnect(SocketChannel channel) {
		new SessionConnectEvent(Core.getServer(getAddress().getPort()), sessions.get(channel), false).call();
	}

	@Override
	public void encode(SocketChannel channel, ByteBuffer buffer) {
		sessions.get(channel).encode(buffer);
	}

	@Override
	public void decode(SocketChannel channel, ByteBuffer buffer) {
		sessions.get(channel).decode(buffer);
	}

}
