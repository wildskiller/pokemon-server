package com.network;

import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import com.network.message.MessageDecoder;
import com.network.message.MessageEncoder;

public abstract class Session {

	private final SocketChannel channel;
	private MessageDecoder decoder;
	private MessageEncoder encoder;

	public Session(SocketChannel channel) {
		this.channel = channel;
	}

	public final Session encode(ByteBuffer buffer) {
		try {
			if (encoder != null) {
				encoder.encode(this, buffer);
				return this;
			}
			channel.write(buffer);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	public final Session decode(ByteBuffer buffer) {
		if (decoder != null)
			try {
				decoder.decode(this, buffer);
			} catch (Exception e) {
				e.printStackTrace();
			}
		return this;
	}

	public SocketChannel getChannel() {
		return channel;
	}

	public MessageDecoder getDecoder() {
		return decoder;
	}

	public Session setDecoder(MessageDecoder decoder) {
		this.decoder = decoder;
		return this;
	}

	public Session setEncoder(MessageEncoder encoder) {
		this.encoder = encoder;
		return this;
	}

	public MessageEncoder getEncoder() {
		return encoder;
	}
}
