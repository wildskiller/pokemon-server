package com.core.tick;

import java.util.concurrent.TimeUnit;

import com.core.server.Server;

/**
 * Represents an interface which can have a tick() method called. This is used for updates and
 * periodic checks.
 * 
 * @author Albert Beaupre
 */
public abstract class ServerTickable implements Runnable {

	protected final Server server;
	private boolean canceled;

	public ServerTickable(Server server) {
		this.server = server;
		canceled = true;
	}

	public void queue() {
		queue(0);
	}

	public void queue(long delay) {
		queue(TimeUnit.MILLISECONDS, delay);
	}

	public void queue(TimeUnit unit, long delay) {
		canceled = false;
		server.getTicker().submit(unit == TimeUnit.MILLISECONDS ? delay : TimeUnit.MILLISECONDS.convert(delay, unit), this);
	}

	public void cancel() {
		canceled = true;
	}

	public boolean isQueued() {
		return !canceled;
	}
}