package com.core.tick;

import com.core.Core;
import com.core.server.Server;

/**
 * @author Albert Beaupre
 */
public class Ticker {

	private final Server server;

	/**
	 * Constructs a new ServerTicker for the given server
	 */
	public Ticker(Server server) {
		this.server = server;
	}

	/**
	 * Submits the given tickable object to the server, allowing it to have it's tick() method
	 * called after the given delay has passed. If the delay is 0, the task will be executed on the
	 * next tick. If the delay is 1, the task will be executed on the tick after the next.
	 * 
	 * @param delay
	 *            the tick delay.
	 * @param t
	 *            the tickable object.
	 * @throws IllegalArgumentException
	 *             if the delay is less than 0.
	 * @throws NullPointerException
	 *             if the tickable object is null
	 */
	public void submit(long delay, Runnable r) {
		Core.submit(server, r, delay, false);
	}

	public Server getServer() {
		return server;
	}

}