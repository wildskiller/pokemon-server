package com.core;

import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;

import com.core.server.Server;
import com.lib.Log;
import com.module.ModuleLoader;
import com.network.ServerHost;
import com.network.message.game.GameHost;

/**
 * Represents the core, responsible for running the server. Should keep this open to allow for
 * running of multiple servers.
 * 
 * This class contains a static thread pool, shared across the JVM.
 * 
 * @author netherfoam
 *
 */
public class Core {

	public static final GameHost DEFAULT_HOST = new GameHost("localhost", 38333);

	/**
	 * A thread pool for handling async tasks that are not on the main server thread.
	 */
	private static ExecutorService threadPool;

	/**
	 * The server that is currently running.
	 */
	private static final HashMap<Integer, Server> servers = new HashMap<>();

	/**
	 * This class loader is used so that the Module system works. It is a shared ClassLoader that
	 * allows the Module system to load classes, independant of where the class is stored. For
	 * example, AIModule should be able to access a class from MusicModule using this ClassLoader.
	 */
	public static final ClassLoader CLASS_LOADER = ClassLoader.getSystemClassLoader();

	public static void loadServer(ServerHost host) {
		Server server = new Server(host);
		servers.put(host.getAddress().getPort(), server);
		submit(null, server, true);
		server.start();
		submit(server, new Runnable() {
			@Override
			public void run() {
				if (host == DEFAULT_HOST) {
					Log.info("Default server booting on port " + host.getAddress().getPort());
					return;
				}

				Log.info("-- New server booting at " + new Date().toString() + " on port " + host.getAddress().getPort() + "--");
			}
		}, false);
	}

	public static void unloadServer(int port) {
		Server server = servers.remove(port);
		server.stop();
	}

	public static void init(int threads, String[] args) {
		try {

			if (threads <= 0)
				threads = Runtime.getRuntime().availableProcessors();
			Log.info("Booting. Core threads: " + threads);
			final long start = System.currentTimeMillis();

			threadPool = Executors.newFixedThreadPool(threads, new ThreadFactory() {
				private int nextThreadId = 0;

				@Override
				public Thread newThread(Runnable r) {
					Thread t = new Thread(r, "ExecutorService " + nextThreadId++);
					t.setContextClassLoader(CLASS_LOADER);
					t.setPriority(Thread.MIN_PRIORITY);
					t.setDaemon(true);
					return t;
				}
			});
			threadPool = Executors.newCachedThreadPool();
			loadServer(DEFAULT_HOST);

			ModuleLoader.get().load();

			// Hint to the garbage collector it should run now.
			System.gc();

			submit(getDefaultServer(), new Runnable() {
				@Override
				public void run() {
					Log.info("Booted in " + (System.currentTimeMillis() - start) + "ms.");
				}
			}, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Submits a Runnable task for execution and returns a Future representing that task. The
	 * Future's get method will return null upon successful completion. The task is scheduled as
	 * soon as possible by the thread pool, and may not be done in sync.
	 * 
	 * @param r
	 *            The runnable task to execute
	 */
	public synchronized static Future<?> submit(Server server, Runnable r, boolean async) {
		if (async)
			return threadPool.submit(r);
		else {
			return server.getServerThread().submit(r);
		}
	}

	/**
	 * Submits the given task for execution after the given number of milliseconds delay. The task
	 * is guaranteed to wait at least delay milliseconds, but is not guaranteed to be executed if
	 * the task list is saturated.
	 * 
	 * @param r
	 *            The runnable
	 * @param delay
	 *            The task delay in milliseconds.
	 */
	public synchronized static Future<Void> submit(Server server, Runnable r, long delay, boolean async) {
		return server.getScheduler().queue(r, delay, async);
	}

	/**
	 * Retrieves the current server that is running.
	 * 
	 * @return the server
	 */
	public static Server getServer(int port) {
		return servers.get(port);
	}

	public static Server getDefaultServer() {
		return servers.get(DEFAULT_HOST.getAddress().getPort());
	}

	public static ExecutorService getThreadPool() {
		return threadPool;
	}

	private Core() {
		// Private Constructor
	}
}
