package com.core.server;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

import com.core.Core;
import com.core.Scheduler;
import com.core.tick.Ticker;
import com.event.EventManager;
import com.network.ServerHost;

/**
 * @author Albert Beaupre
 */
public class Server implements Runnable {
	/**
	 * Handles scheduling of tasks for a later date, optionally on the main thread or on an async
	 * thread
	 */
	private Scheduler scheduler;

	private final ServerHost host;
	private final ServerThread serverThread;
	private ServerSocketChannel server;

	private EventManager eventManager;
	private Ticker ticker;

	public Server(ServerHost host) {
		this.host = host;
		this.serverThread = new ServerThread(this);
		this.eventManager = new EventManager(this);
		this.ticker = new Ticker(this);
		this.scheduler = new Scheduler(serverThread, Core.getThreadPool());
	}

	@Override
	public void run() {
		try {
			server = ServerSocketChannel.open();
			server.configureBlocking(false);
			server.socket().bind(host.getAddress());
			Selector selector = Selector.open();
			server.register(selector, SelectionKey.OP_ACCEPT);

			while (selector.isOpen()) {
				selector.select();
				Iterator<SelectionKey> i = selector.selectedKeys().iterator();
				while (i.hasNext()) {
					SelectionKey key = (SelectionKey) i.next();
					i.remove();
					if (key.isAcceptable()) {
						SocketChannel client = server.accept();
						client.configureBlocking(false);
						client.register(selector, SelectionKey.OP_READ);
						host.connect(client);
						continue;
					}
					if (key.isReadable()) {
						SocketChannel client = (SocketChannel) key.channel();
						ByteBuffer buffer = ByteBuffer.allocate(4096);
						try {
							int numRead = client.read(buffer);
							if (numRead > 0) {
								byte[] data = new byte[numRead];
								System.arraycopy(buffer.array(), 0, data, 0, numRead);
								host.decode(client, ByteBuffer.wrap(data));
							}
						} catch (Exception e) {
							e.printStackTrace();
							host.disconnect(client);
							client.close();
							continue;
						}
						continue;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void start() {
		serverThread.start();
	}

	public void stop() {
		try {
			server.close();
			serverThread.shutdown();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ServerThread getServerThread() {
		return serverThread;
	}

	public ServerHost getHost() {
		return host;
	}

	public EventManager getEvents() {
		return eventManager;
	}

	public Ticker getTicker() {
		return ticker;
	}

	public Scheduler getScheduler() {
		return scheduler;
	}

	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}
}
