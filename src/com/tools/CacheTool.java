package com.tools;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import javax.imageio.ImageIO;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import com.cache.CacheStore;
import com.cache.Child;
import com.cache.ChildTable;

@SuppressWarnings("serial")
public class CacheTool extends JFrame {

	private final JFileChooser chooser = new JFileChooser();
	private File currentFile;
	private DefaultMutableTreeNode cacheStoreNode;
	private CacheStore currentCacheStore;

	JScrollPane fileViewScrollPane = new JScrollPane();
	JTextArea rawFileTextArea = new JTextArea();
	private JTree childTree;

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CacheTool frame = new CacheTool();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CacheTool() {
		setBackground(Color.WHITE);
		setTitle("Cache Tool");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);

		JMenuItem menuItemOpen = new JMenuItem("Open...");
		menuItemOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int option = chooser.showOpenDialog(CacheTool.this);
				if (option == JFileChooser.APPROVE_OPTION) {
					currentFile = chooser.getSelectedFile();
					try {
						loadFromFile(currentFile);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		menuItemOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		menuItemOpen.setBackground(Color.WHITE);
		mnFile.add(menuItemOpen);

		JMenuItem menuItemSave = new JMenuItem("Save");
		menuItemSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (currentFile != null) {
					try {
						currentCacheStore.writeToFile(currentFile);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				} else {
					int option = chooser.showSaveDialog(CacheTool.this);
					if (option == JFileChooser.APPROVE_OPTION) {
						currentFile = chooser.getSelectedFile();
						try {
							currentCacheStore.writeToFile(currentFile);
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}
				}

			}
		});
		menuItemSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		mnFile.add(menuItemSave);

		JMenuItem mntmSaveAs = new JMenuItem("Save As...");
		mntmSaveAs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int option = chooser.showSaveDialog(CacheTool.this);
				if (option == JFileChooser.APPROVE_OPTION) {
					currentFile = chooser.getSelectedFile();
					try {
						currentCacheStore.writeToFile(currentFile);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		mntmSaveAs.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		mnFile.add(mntmSaveAs);
		contentPane = new JPanel();
		contentPane.setBackground(Color.LIGHT_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JScrollPane childScrollPane = new JScrollPane();

		JScrollPane previewScrollPane = new JScrollPane();

		JLabel previewHeaderLabel = new JLabel("Preview");
		previewHeaderLabel.setFont(new Font("Arial", Font.PLAIN, 18));
		previewHeaderLabel.setHorizontalAlignment(SwingConstants.CENTER);
		previewScrollPane.setColumnHeaderView(previewHeaderLabel);

		JLabel jTreeHeaderLabel = new JLabel("Children");
		jTreeHeaderLabel.setFont(new Font("Arial", Font.PLAIN, 18));
		jTreeHeaderLabel.setHorizontalAlignment(SwingConstants.CENTER);
		childScrollPane.setColumnHeaderView(jTreeHeaderLabel);

		childTree = new JTree();
		childTree.setModel(new DefaultTreeModel(this.cacheStoreNode = new DefaultMutableTreeNode(this.currentCacheStore = new CacheStore())));

		childTree.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				TreePath path = childTree.getPathForLocation(e.getX(), e.getY());
				if (path != null) {
					childTree.setSelectionPath(path);
					DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
					if (SwingUtilities.isLeftMouseButton(e)) {
						if (node.getUserObject() instanceof Child) {
							Child child = (Child) node.getUserObject();

							if (e.getClickCount() == 2)
								viewChild(child);
						}
					} else if (SwingUtilities.isRightMouseButton(e)) {
						if (node != null) {
							if (node.getUserObject() instanceof ChildTable) {
								JPopupMenu menu = new JPopupMenu();
								JMenuItem addChild = new JMenuItem("Add Child");
								addChild.addActionListener(new ActionListener() {
									@Override
									public void actionPerformed(ActionEvent e) {
										ChildTable table = (ChildTable) node.getUserObject();
										Child child = new Child(table.missingChildIndex(), new byte[0]);
										DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(child);
										table.addChild(child);
										node.add(childNode);
										((DefaultTreeModel) childTree.getModel()).reload(node);
										childTree.expandPath(path);
									}
								});
								menu.add(addChild);
								menu.show(childTree, e.getX(), e.getY());
							} else if (node.getUserObject() instanceof CacheStore) {
								JPopupMenu menu = new JPopupMenu();
								JMenuItem addTable = new JMenuItem("Add Table");
								addTable.addActionListener(new ActionListener() {
									@Override
									public void actionPerformed(ActionEvent e) {
										addTable(new ChildTable(currentCacheStore.getTables().size()));
									}
								});
								menu.add(addTable);
								menu.show(childTree, e.getX(), e.getY());
							} else if (node.getUserObject() instanceof Child) {
								Child child = (Child) node.getUserObject();
								JPopupMenu menu = new JPopupMenu();
								JMenuItem addFile = new JMenuItem("Set File");
								JMenuItem delete = new JMenuItem("Delete Child");
								delete.addActionListener(new ActionListener() {
									@Override
									public void actionPerformed(ActionEvent e) {
										DefaultTreeModel model = (DefaultTreeModel) childTree.getModel();

										ChildTable parent = (ChildTable) ((DefaultMutableTreeNode) node.getParent()).getUserObject();
										model.removeNodeFromParent(node);
										parent.removeChild(child.getId());
									}
								});
								addFile.addActionListener(new ActionListener() {
									@Override
									public void actionPerformed(ActionEvent e) {
										int option = chooser.showSaveDialog(CacheTool.this);
										if (option == JFileChooser.APPROVE_OPTION) {
											try {
												child.loadDataFromFile(chooser.getSelectedFile());
											} catch (Exception e1) {
												e1.printStackTrace();
											}
										}
									}
								});
								JMenuItem viewFile = new JMenuItem("View Child");
								viewFile.addActionListener(new ActionListener() {
									@Override
									public void actionPerformed(ActionEvent e) {
										viewChild(child);
									}
								});
								menu.add(delete);
								menu.add(viewFile);
								menu.add(addFile);
								menu.show(childTree, e.getX(), e.getY());
							}
						}
					}
				}
			}
		});
		childScrollPane.setViewportView(childTree);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPane.createSequentialGroup().addGap(10).addComponent(childScrollPane, GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE).addGap(18).addComponent(previewScrollPane, GroupLayout.DEFAULT_SIZE, 517, Short.MAX_VALUE).addContainerGap()));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPane.createSequentialGroup().addGap(11).addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING).addComponent(previewScrollPane, Alignment.LEADING).addComponent(childScrollPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 508, Short.MAX_VALUE)).addGap(11)));

		JPanel panel = new JPanel();
		previewScrollPane.setViewportView(panel);

		JScrollPane scrollPane = new JScrollPane();

		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(gl_panel.createParallelGroup(Alignment.TRAILING).addGroup(gl_panel.createSequentialGroup().addContainerGap().addGroup(gl_panel.createParallelGroup(Alignment.TRAILING).addComponent(fileViewScrollPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE).addComponent(scrollPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE)).addContainerGap()));
		gl_panel.setVerticalGroup(gl_panel.createParallelGroup(Alignment.TRAILING).addGroup(gl_panel.createSequentialGroup().addContainerGap().addComponent(fileViewScrollPane, GroupLayout.DEFAULT_SIZE, 335, Short.MAX_VALUE).addPreferredGap(ComponentPlacement.UNRELATED).addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE).addContainerGap()));

		JLabel lblNewLabel = new JLabel("File View");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		fileViewScrollPane.setColumnHeaderView(lblNewLabel);

		JLabel lblRawFile = new JLabel("Raw File");
		lblRawFile.setHorizontalAlignment(SwingConstants.CENTER);
		scrollPane.setColumnHeaderView(lblRawFile);
		scrollPane.setViewportView(rawFileTextArea);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);
	}

	public void viewChild(Child child) {
		ByteArrayInputStream byteStream = new ByteArrayInputStream(child.getData());
		InputStreamReader byteReader = new InputStreamReader(byteStream);
		BufferedReader reader = new BufferedReader(byteReader);
		StringBuilder builder = new StringBuilder();
		try {
			String line;
			while ((line = reader.readLine()) != null)
				builder.append('\n' + line);
			reader.close();
			rawFileTextArea.setText(builder.toString());

			byteStream.reset();
			BufferedImage image = ImageIO.read(byteStream);
			if (image != null) {
				JLabel lbl = new JLabel();
				lbl.setIcon(new ImageIcon(image));
				fileViewScrollPane.setViewportView(lbl);
				fileViewScrollPane.invalidate();
				return;
			} else {
				fileViewScrollPane.setViewportView(new JLabel());
				fileViewScrollPane.invalidate();
			}
			byteStream.reset();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void loadFromFile(File file) {
		try {
			CacheStore store = CacheStore.load(new FileInputStream(file));
			this.cacheStoreNode = new DefaultMutableTreeNode(store);
			for (ChildTable table : store.getTables()) {
				DefaultMutableTreeNode tableNode = new DefaultMutableTreeNode(table);
				for (Child child : table.getChildren()) {
					DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(child);
					tableNode.add(childNode);
				}
				cacheStoreNode.add(tableNode);
			}
			childTree.setModel(new DefaultTreeModel(this.cacheStoreNode));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void addTable(ChildTable table) {
		this.currentCacheStore.addTable(table);
		DefaultMutableTreeNode tableNode = new DefaultMutableTreeNode(table);
		this.cacheStoreNode.add(tableNode);
		((DefaultTreeModel) this.childTree.getModel()).insertNodeInto(tableNode, cacheStoreNode, table.getId());
		this.childTree.expandRow(0);
	}
}
