package com.examples;

import com.core.Core;
import com.core.tick.ServerTickable;

public class TickableExample {

	public static void main(String[] args) {
		Core.init(Runtime.getRuntime().availableProcessors() - 1, args);

		for (int i = 0; i < 1000; i++) {
			new ServerTickable(Core.getDefaultServer()) {
				int count;

				@Override
				public void run() {
					if (count++ >= 2) {
						cancel();
						return;
					}
					System.out.println("TICKING");
					queue(1000);
				}

			}.queue(1000);
		}
	}

}
