package com.examples;

import java.io.File;

import com.lib.config.ConfigSection;
import com.lib.config.FileConfig;

public class YMLExample {

	public static void main(String[] args) {

		FileConfig config = new FileConfig(new File("./YMLExample.yml")); // This class can save and load any YML files as well as write/read to/from them

		class Location {
			@SuppressWarnings("unused") public int x = 3333, y = 3222, z = 0;
		} // useless class for example

		ConfigSection locationConfig = new ConfigSection(); // This class makes reading/writing values easyHashMap
		locationConfig.set("coords", new Location());

		/**
		 * SAVING VARIABLES
		 */
		config.set("example1", false);
		config.set("example2", 12312);
		config.set("example3", "string here");
		config.set("example4", new Object[] { "arrayElement1", 2, true });
		config.set("location", locationConfig);
		config.save(); // Saves to the YMLExample.yml file in a yml format using
						// the hashmap within the class
		/**
		 * DONE SAVING VARIABLES
		 */

		ConfigSection loadedConfigs = new ConfigSection();
		config.load(new File("./YMLExample.yml"), loadedConfigs); // loads the YML from the specified file and imports it into the specified map -> loadedConfigs

		System.out.println(loadedConfigs);
	}

}
