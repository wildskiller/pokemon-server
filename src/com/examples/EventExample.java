package com.examples;

import com.core.Core;
import com.core.tick.ServerTickable;
import com.event.Event;
import com.event.EventHandler;
import com.event.EventListener;
import com.event.EventPriority;

public class EventExample {

	public static void main(String[] args) {
		Core.init(Runtime.getRuntime().availableProcessors() - 1, args);

		class UpdateEvent extends Event {
			final long untilUpdate;

			UpdateEvent(long untilUpdate) {
				super(Core.getDefaultServer());
				this.untilUpdate = untilUpdate;
			}

			long getUntilUpdate() {
				return untilUpdate;
			}

		}

		EventListener listener = new EventListener() {
			@EventHandler()
			// This must be added to any method that you want to listen for
			// events
			public void whatever(Event event) { // this method can have any name
				/**
				 * This method can have any name but wont be called unless the only parameter is an
				 * instant of Event.
				 * 
				 * If an Event that is called is an instance of the parameter, then this method will
				 * be executed.
				 * 
				 * So, if I have a MapUpdateEvent and my parameter is whatever(MapUpdateEvent
				 * event), then whenever MapUpdateEvent is called anywhere...this method is
				 * executed.
				 * 
				 * Now... since every event will have an instance of Event, this method will be
				 * executed upon any event call. That is exactly why it is good to isolate events.
				 */
				System.out.println(event.getClass().getName() + " has been called");
			}

			@EventHandler(priority = EventPriority.MAXIMUM)
			public void onUpdate(UpdateEvent event) {
				/**
				 * This method will only be called if an UpdateEvent is called.
				 */
				System.out.println("Update will occur in " + event.getUntilUpdate() + "ms.");
				new ServerTickable(Core.getDefaultServer()) {
					public void run() {
						System.out.println("Updating!");
					}
				}.queue(event.getUntilUpdate()); // queues this tickable in the update event time
			}
		};
		listener.register(Core.getDefaultServer()); // Registers this listener so it can listen for events

		UpdateEvent updateEvent = new UpdateEvent(3000);
		Event event = new Event(Core.getDefaultServer()); // Extend an Event class to create different instances if you want to isolate different events
		event.call(); // calls this event and the EventManager inside the Core and any EventListeners will listen for this type of event
		updateEvent.call(); // calls the updateEvent for the EventManager to listen for any events of this type

	}

}
