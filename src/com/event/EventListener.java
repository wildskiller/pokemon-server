package com.event;

import com.core.server.Server;

/**
 * @author Albert Beaupre
 */
public interface EventListener {

	/**
	 * Registers this {@code EventListener} to the {@code EventManager} inside {@code Core}.
	 */
	default void register(Server server) {
		server.getEvents().register(this);
	}

	/**
	 * Unregisters this {@code EventListener} from the {@code EventManager} inside {@code Core}.
	 */
	default void unregister(Server server) {
		server.getEvents().unregister(this);
	}

}
