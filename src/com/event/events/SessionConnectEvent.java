package com.event.events;

import com.core.server.Server;
import com.event.Event;
import com.network.Session;

public class SessionConnectEvent extends Event {

	private final Session session;
	private final boolean establishing;

	public SessionConnectEvent(Server server, Session session, boolean establishing) {
		super(server);
		this.session = session;
		this.establishing = establishing;
	}

	public Session getSession() {
		return session;
	}

	public boolean isEstablishing() {
		return establishing;
	}

}
