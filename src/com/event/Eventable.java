package com.event;

import com.core.server.Server;

public interface Eventable {


	/**
	 * Registers the specified {@code eventListener} from the {@code EventManager} inside {@code Core}.
	 */
	default void register(EventListener eventListener) {
		getServer().getEvents().register(eventListener);
	}

	/**
	 * Unregisters the specified {@code eventListener} from the {@code EventManager} inside {@code Core}.
	 */
	default void unregister(EventListener eventListener) {
		getServer().getEvents().unregister(eventListener);
	}
	
	Server getServer();
	
}
