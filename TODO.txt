I want content creation to be like this:
	
	void runScript() {
		teleport(3222, 3222, 0); //teleports the script owner to 3222, 3222, 0
		wait(2); //waits 1 tick before the script continues
		animate(2222); // the script owner animates a thinking animation, but I don't think 2222 is the right one :/
		forceTalk("Huh...Where am I?"); //forces the script owner to force chat the specified string
		wait(3);
		walk(3200, 3200); //walks the script owner to 3200, 3200 and pauses the script until the script owner reaches the end of the path
		//And so on...
		NPC nearby = getNearby(NPC.class, 10).get(0); //retrieves a list of nearby entities within a radius of 10 instancing NPC and gets the first element in the list
		boolean lookingForEnemies = nearby != null;
		while (!(lookingForEnemies = (nearby = (getNearby(NPC.class, 10).get(0) != null))))
			walk(3222 + random(100), 3222 + random(100)); //walks the script owner to a random location if there are no nearby enemies
	}